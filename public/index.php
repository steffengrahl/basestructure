<?php
session_start();

require_once __DIR__ . '/../inc/config.php';

if (getenv('APP_ENV') === 'dev') {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
}

require_once __DIR__ . '/../inc/site.php';

$site   = $_GET['site'] ?? 'start';
$output = __DIR__ . '/../pages/' . $site . '.php';

if (!file_exists($output)) {
    $output = __DIR__ . '/../pages/404.php';
}

require_once $output;

include __DIR__ . '/../templates/base.tpl.php';
