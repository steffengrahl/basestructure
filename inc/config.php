<?php
$envFile = __DIR__ . '/../.env';

if (is_readable($envFile)) {
    $envItems = file($envFile);
    
    foreach ($envItems as $envItem) {
        $envItem = trim($envItem);
        
        if (empty($envItem)) {
            continue;
        }
        
        putenv($envItem);
    }
}

if (getenv('DATABASE_DRIVER') === 'sqlite') {
    $dsn = sprintf(
        '%s:%s',
        getenv('DATABASE_DRIVER'),
        getenv('DATABASE_HOST')
    );
    
    try {
        $pdo = new PDO($dsn);
    } catch (Exception $exception) {
        var_dump($exception->getMessage());
    }
}

if (getenv('DATABASE_DRIVER') === 'mysql') {
    $dsn = sprintf(
        '%s:host=%s;database=%s',
        getenv('DATABASE_DRIVER'),
        getenv('DATABASE_HOST'),
        getenv('DATABASE_NAME')
    );
    
    try {
        $pdo = new PDO($dsn, getenv('MYSQL_USER'), getenv('MYSQL_PASSWORD'));
    } catch (Exception $exception) {
        var_dump($exception->getMessage());
    }
}
