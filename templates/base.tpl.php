<!doctype html>
<html lang="<?= getenv('LOCALE') ?>">
<head>
    <title><?= prepareHeadTitle($page['title']) ?></title>
    <meta charset="<?= getenv('CHARSET') ?>">
</head>
<body>
    <header>
        <h1><?= $page['title'] ?></h1>
    </header>
    <aside>
        <nav>
            <?php include __DIR__ . '/navigation.tpl.php' ?>
        </nav>
    </aside>
    <main>
        <?php include __DIR__ . '/../templates/' . $template . '.tpl.php' ?>
    </main>
    <footer>
        <p>copyright <?= (new DateTime())->format('Y') ?></p>
    </footer>
</body>
</html>
